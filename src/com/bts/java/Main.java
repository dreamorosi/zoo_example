package com.bts.java;

import com.bts.java.model.Animal;
import com.bts.java.model.Fish;
import com.bts.java.model.Mammal;
import com.bts.java.model.PetBird;
import com.bts.java.model.Reptile;
import com.bts.java.model.WildBird;

public class Main {

	public static void main(String[] args) {
		System.out.println("Welcome to the Zoo!");
		
		Animal myZoo[];
		myZoo = new Animal[10];
		
		System.out.println("Number of animals in the Zoo: " + Animal.getCount());
		System.out.println("Let's put some animals in this Zoo!");
		
		myZoo[0] = new Mammal("bau bau", 68);
		myZoo[1] = new Mammal("meow mew", 60);
		myZoo[2] = new PetBird(true, "Round", "Yellow", 12);
		myZoo[3] = new WildBird(false, "Pointy", "Black", "Ice");
		myZoo[4] = new Reptile(false);
		myZoo[5] = new Reptile(true);
		myZoo[6] = new Fish(true, false);
		myZoo[7] = new Fish(false, true);
		myZoo[8] = new Fish(true, true);
		myZoo[9] = new Fish(false, false);
		
		System.out.println("Number of animals in the Zoo: " + Animal.getCount());
		System.out.println("Let's look at the animals one by one..");
		for (Animal element : myZoo) {
			System.out.println(element.growl());
		}
		
		System.out.println("Done!");
	}

}
