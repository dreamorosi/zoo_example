package com.bts.java.model;

public class Fish extends Animal {
	private boolean hasGills;
	private boolean hasScales;

	public Fish(boolean p1, boolean p2) {
		super("bubble bubble");
		this.hasGills = p1;
		this.hasScales = p2;
	}

	public boolean isHasGills() {
		return hasGills;
	}

	public void setHasGills(boolean hasGills) {
		this.hasGills = hasGills;
	}

	public boolean isHasScales() {
		return hasScales;
	}

	public void setHasScales(boolean hasScales) {
		this.hasScales = hasScales;
	}

	@Override
	public String growl() {
		return this.getClass().getSimpleName() + " says " + super.getSound() + "!";
	}

}
