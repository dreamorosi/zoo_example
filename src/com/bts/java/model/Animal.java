package com.bts.java.model;

public abstract class Animal {
	private String name;
	private String species;
	private int age;
	private static int count;
	private String color;
	private boolean gender;
	private int location;
	private String sound;

	public Animal(String p1) {
		Animal.count = Animal.count + 1;
		System.out.println("A wild " + this.getClass().getSimpleName() + " appears!");
		this.sound = p1;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSpecies() {
		return species;
	}

	public void setSpecies(String species) {
		this.species = species;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public static int getCount() {
		return count;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public boolean isGender() {
		return gender;
	}

	public void setGender(boolean gender) {
		this.gender = gender;
	}

	public int getLocation() {
		return location;
	}

	public void setLocation(int location) {
		this.location = location;
	}
	
	public void setSound(String sound) {
		this.sound = sound;
	}
	
	public String getSound() {
		return this.sound;
	}
	
	public abstract String growl();
}
