package com.bts.java.model;

public class PetBird extends Bird {
	private int cageSize;

	public PetBird(boolean p0, String p2, String p3, int p4) {
		super(p0, p2, p3);
		this.cageSize = p4;
	}

	public int getCageSizeInt() {
		return cageSize;
	}
	
	public String getCageSize() {
		return cageSize + "cm2";
	}

	public void setCageSize(int cageSize) {
		this.cageSize = cageSize;
	}
}
