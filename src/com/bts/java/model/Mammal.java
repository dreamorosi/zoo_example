package com.bts.java.model;

public class Mammal extends Animal {
	private int gestationLength;

	public Mammal(String p1, int p2) {
		super(p1);
		this.gestationLength = p2;
	}

	public String getGestationLength() {
		return gestationLength + " days";
	}

	public void setGestationLength(int gestationLength) {
		this.gestationLength = gestationLength;
	}

	@Override
	public String growl() {
		return this.getClass().getSimpleName() + " says " + super.getSound() + "!";
	}

}
