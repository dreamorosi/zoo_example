package com.bts.java.model;

public class WildBird extends Bird {
	private String habitatType;

	public WildBird(boolean p0, String p2, String p3, String p4) {
		super(p0, p2, p3);
		this.habitatType = p4;
	}
	
	public String getHabitatType() {
		return habitatType;
	}

	public void setHabitatType(String habitatType) {
		this.habitatType = habitatType;
	}
}
