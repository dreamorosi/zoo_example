package com.bts.java.model;

public class Bird extends Animal {
	private boolean isFlying;
	private String beakShape;
	private String mainColor;
	
	public Bird(boolean p0, String p2, String p3) {
		super("chip chip");
		this.isFlying = p0;
		this.beakShape = p2;
		this.mainColor = p3;
	}
	
	public boolean isFlying() {
		return isFlying;
	}

	public void setFlying(boolean isFlying) {
		this.isFlying = isFlying;
	}

	public String getBeakShape() {
		return beakShape;
	}

	public void setBeakShape(String beakShape) {
		this.beakShape = beakShape;
	}

	public String getMainColor() {
		return mainColor;
	}

	public void setMainColor(String mainColor) {
		this.mainColor = mainColor;
	}

	@Override
	public String growl() {
		return this.getClass().getSimpleName() + " says " + super.getSound() + "!";
	}
}
