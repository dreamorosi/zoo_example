package com.bts.java.model;

public class Reptile extends Animal {
	private boolean hasLegs;

	public Reptile(boolean p1) {
		super("???");
		this.hasLegs = p1;
	}
	
	public boolean isHasLegs() {
		return hasLegs;
	}

	public void setHasLegs(boolean hasLegs) {
		this.hasLegs = hasLegs;
	}


	@Override
	public String growl() {
		return this.getClass().getSimpleName() + " says " + super.getSound() + "!";
	}
}
